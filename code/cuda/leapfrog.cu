#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define BLOCK_SIZE 128
//#define EPSILON 1e20f
#define EPSILON 1
//#define G 6.67430e-11f
#define G 1

__global__
void bodyForce(float4 *pos, float4 *vel, float dt, int n) {
  int c = blockDim.x * blockIdx.x + threadIdx.x;

  if (c < n) {
    float fX = 0.0f; 
    float fY = 0.0f; 
    float fZ = 0.0f;

    for (int i = 0; i < gridDim.x; i++) {
      // Allocate shared memory to use across different threads
      __shared__ float4 spos[BLOCK_SIZE];

      // Each thread loads their part of the shared memory
      spos[threadIdx.x] = pos[i * blockDim.x + threadIdx.x];

      // Wait until all threads finished loading shared memory
      __syncthreads();

      for (int j = 0; j < BLOCK_SIZE; j++) {
        float diffX = spos[j].x - pos[i].x;
        float diffY = spos[j].y - pos[i].y;
        float diffZ = spos[j].z - pos[i].z;

        float distSqr = diffX * diffX + diffY * diffY + diffZ * diffZ + EPSILON;
        float invDist = rsqrtf(distSqr);
        float invDist3 = invDist * invDist * invDist;

        float partialForce = G * spos[j].w * invDist3;

        fX += diffX * partialForce; 
        fY += diffY * partialForce; 
        fZ += diffZ * partialForce;
      }
      __syncthreads();
    }

    vel[c].x += dt * fX; 
    vel[c].y += dt * fY; 
    vel[c].z += dt * fZ;
  }
}

// Based on loadData from https://www.evl.uic.edu/sjames/cs525/project2.html
void loadFromFile(float4* pos, float4* vel, const char* filename, int bodies) {
  //const float posFactor = 4 * 3.086e19f;
  //const float velFactor = 220 * 1000;
  //const float massFactor = 5.36585e10f * 2e30f;
  const float posFactor = 1;
  const float velFactor = 1;
  const float massFactor = 1;
  const int totalBodies = 81920;

  const int skip = totalBodies / bodies;
  
  // Max length of a line
  const int MAXSTR = 200;

  FILE *fin;

  if ((fin = fopen(filename, "r"))) {
    char buf[MAXSTR];
    float line[7];

    // Allocate memory
    pos = (float4*)malloc(sizeof(float4)*bodies);
    vel = (float4*)malloc(sizeof(float4)*bodies);

    int k = 0;
    for (int i=0; i < bodies; i++, k++) {

      // Skip some bodies
      for (int j=0; j < skip; j++, k++) {
        fgets (buf, MAXSTR, fin);
      }

      sscanf(buf, "%f %f %f %f %f %f %f", 
             line + 0, line + 1, line + 2, line + 3, line + 4, line + 5, line + 6);


      // Position
      pos[i].x = line[1] * posFactor;
      pos[i].y = line[2] * posFactor;
      pos[i].z = line[3] * posFactor;

      // Mass
      pos[i].w = line[0] * massFactor;

      // Velocity
      vel[i].x = line[4] * velFactor;
      vel[i].y = line[5] * velFactor;
      vel[i].z = line[6] * velFactor;
      vel[i].w = 1.0f;
    }
  }
  else
  {
    printf("Unable to read from %s\n", filename);
    exit(0);
  }
}

int main(const int argc, const char** argv) {
  int totalBodies = 30000; 
  if (argc > 1) totalBodies = atoi(argv[1]);

  long long dt = 157680000000000L; // 5 million years
  if (argc > 2) dt = atol(argv[2]);

  int totalIterations = 100;
  if (argc > 3) totalIterations = atoi(argv[3]);

  const char* filename = "code/data/dubinski.tab.gz";
  
  float4* pos;
  float4* vel;
  loadFromFile(pos, vel, filename, totalBodies);

  int bytes = totalBodies * sizeof(float4);

  float4* d_pos;
  float4* d_vel;
  cudaMalloc(&d_pos, bytes);
  cudaMalloc(&d_vel, bytes);

  int totalBlocks = (totalBodies + BLOCK_SIZE - 1) / BLOCK_SIZE;

  for (int iter = 1; iter <= totalIterations; iter++) {

    cudaMemcpy(d_pos, pos, bytes, cudaMemcpyHostToDevice);
    cudaMemcpy(d_vel, vel, bytes, cudaMemcpyHostToDevice);

    bodyForce<<<totalBlocks, BLOCK_SIZE>>>(d_pos, d_vel, dt, totalBodies);

    cudaMemcpy(pos, d_pos, bytes, cudaMemcpyDeviceToHost);
    cudaMemcpy(vel, d_vel, bytes, cudaMemcpyDeviceToHost);

    for (int i = 0 ; i < totalBodies; i++) {
      pos[i].x += vel[i].x * dt;
      pos[i].y += vel[i].y * dt;
      pos[i].z += vel[i].z * dt;
    }
  }

  free(pos);
  free(vel);
  cudaFree(d_pos);
  cudaFree(d_vel);
}